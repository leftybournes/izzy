((nil . ((lice:copyright-holder . "Kent Delante <kdeleteme@tutanota.com>")))
 (c-mode . ((flycheck-clang-include-path
             . ("/usr/include/gtk-3.0"
                "/usr/include/glib-2.0"
                "/usr/lib64/glib-2.0/include"
                "/usr/include/pango-1.0"
                "/usr/include/cairo"
                "/usr/include/gdk-pixbuf-2.0"
                "/usr/include/atk-1.0"))
            (indent-tabs-mode . t))))

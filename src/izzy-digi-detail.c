/* izzy-digi-detail.c - Shows the details of a chosen digimon
 *
 * Copyright (C) 2019  Kent Delante Kent Delante <kdeleteme@tutanota.com>
 *
 * This file is part of Izzy.
 *
 * Izzy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Izzy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Izzy. If not, see <http://www.gnu.org/licenses/>.
 */


#include "config.h"

#include <sqlite3.h>

#include "izzy-digivolution.h"
#include "izzy-digi-detail.h"
#include "izzy-digi-location.h"

struct _IzzyDigiDetail {
	GtkBox parent_instance;

	guint id;
	GtkWidget *name;
	GtkWidget *digi_type;
	GtkWidget *rank;
	GtkWidget *specialty;
	GtkWidget *list_location;
	GtkWidget *list_digivolution;
};

G_DEFINE_TYPE (IzzyDigiDetail, izzy_digi_detail, GTK_TYPE_BOX);

GtkWidget *izzy_digi_detail_new(void) {
	return GTK_WIDGET(g_object_new(IZZY_TYPE_DIGI_DETAIL, NULL));
}

static void show_locations(IzzyDigiDetail *self, IzzyDigiStore *store,
						   long digimon_id) {
	
	GList *locations = izzy_digi_store_get_locations(store, digimon_id);

	// TODO: Use a list box instead of a grid

	/* guint row = 1; */

	/* for (GList *l = locations; l != NULL; l = l->next) { */
	/* 	gtk_grid_insert_row(GTK_GRID(self->list_location), row); */

	/* 	IzzyDigiLocation *location = (IzzyDigiLocation *) l->data; */
		
	/* 	GtkWidget *domain = gtk_label_new(location->domain); */
	/* 	g_object_set(domain, "expand", TRUE, NULL); */
	/* 	gtk_grid_attach(GTK_GRID(self->list_location), domain, 0, row, 1, 1); */

	/* 	GtkWidget *floors = gtk_label_new(location->floors); */
	/* 	g_object_set(floors, "expand", TRUE, NULL); */
	/* 	gtk_grid_attach(GTK_GRID(self->list_location), floors, 1, row, 1, 1); */

	/* 	gchar *after_bk; */

	/* 	if (location->after_bk) after_bk = "Yes"; */
	/* 	else after_bk = "No"; */

	/* 	GtkWidget *label_after_bk = gtk_label_new(after_bk); */
	/* 	g_object_set(label_after_bk, "expand", TRUE, NULL); */
	/* 	gtk_grid_attach(GTK_GRID(self->list_location), label_after_bk, 2, row, */
	/* 					1, 1); */
		
	/* 	++row; */
	/* } */
	
	gtk_widget_show_all(self->list_location);
}

static void show_digivolutions(IzzyDigiDetail *self, IzzyDigiStore *store,
							   long digimon_id) {
	GList *digivolutions = izzy_digi_store_get_digivolutions(store,
															 digimon_id);

	for (GList *d = digivolutions; d != NULL; d = d->next) {
		GtkWidget *row = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);

		IzzyDigivolution *digivolution = (IzzyDigivolution *) d->data;

		gchar *dp_label;
		if (digivolution->dp_max)
			dp_label = g_strdup_printf("%d-%d", digivolution->dp_min,
									   digivolution->dp_max);
		else dp_label = g_strdup_printf("%d+", digivolution->dp_min);

		GtkWidget *dp = gtk_label_new(dp_label);
		gtk_box_pack_start(GTK_BOX(row), dp, TRUE, TRUE, 0);

		GtkWidget *next = gtk_label_new(digivolution->next);
		gtk_box_pack_start(GTK_BOX(row), next, TRUE, TRUE, 0);

		gtk_list_box_insert(GTK_LIST_BOX(self->list_digivolution), row, -1);
	}
}

void izzy_digi_detail_load_info(IzzyDigiDetail *self, IzzyDigiStore *store,
								long digimon_id) {
	/* show_locations(self, store, digimon_id); */
	show_digivolutions(self, store, digimon_id);
	// TODO: show_dnadv_results(self, store, digimon_id);
	// TODO: show_combinations(self, store, digimon_id);

	gtk_widget_show_all(GTK_WIDGET(self));
}

static void clear_info_callback(GtkWidget *widget, gpointer data) {
	gtk_widget_destroy(widget);
}

void izzy_digi_detail_clear_info(IzzyDigiDetail *self) {
	gtk_container_forall(GTK_CONTAINER(self->list_digivolution),
						 clear_info_callback, NULL);
}

static void izzy_digi_detail_init(IzzyDigiDetail *self) {
	gtk_widget_init_template(GTK_WIDGET(self));
}

static void izzy_digi_detail_class_init(IzzyDigiDetailClass *klass) {
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);

	gtk_widget_class_set_template_from_resource(widget_class,
												"/com/gitlab/kdeleteme/izzy"
												"/ui/izzy-digi-detail.ui");

	gtk_widget_class_bind_template_child(widget_class, IzzyDigiDetail, name);
	gtk_widget_class_bind_template_child(widget_class, IzzyDigiDetail,
										 digi_type);
	gtk_widget_class_bind_template_child(widget_class, IzzyDigiDetail, rank);
	gtk_widget_class_bind_template_child(widget_class, IzzyDigiDetail,
										 specialty);
	gtk_widget_class_bind_template_child(widget_class, IzzyDigiDetail,
										 list_location);
	gtk_widget_class_bind_template_child(widget_class, IzzyDigiDetail,
										 list_digivolution);	
}

void izzy_digi_detail_set_id(GtkWidget *self, const guint id) {
	((IzzyDigiDetail *) self)->id = id;
}

void izzy_digi_detail_set_name(GtkWidget *self, const gchar *name) {
	gchar *label_text = g_markup_printf_escaped("<b><big>%s</big></b>", name);
	gtk_label_set_markup(GTK_LABEL(((IzzyDigiDetail *) self)->name),
						 label_text);
}

void izzy_digi_detail_set_type(GtkWidget *self, const gchar *digi_type) {
	gtk_label_set_text(GTK_LABEL(((IzzyDigiDetail *) self)->digi_type),
					   digi_type);
}
void izzy_digi_detail_set_rank(GtkWidget *self, const gchar *rank) {
	gtk_label_set_text(GTK_LABEL(((IzzyDigiDetail *) self)->rank), rank);
}
void izzy_digi_detail_set_specialty(GtkWidget *self, const gchar *specialty) {
	gtk_label_set_text(GTK_LABEL(((IzzyDigiDetail *) self)->specialty),
					   specialty);
}


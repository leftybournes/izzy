/* izzy-window.c
 *
 * Copyright (C) 2019  Kent Delante <kdeleteme@tutanota.com>
 *
 * This file is part of Izzy.
 *
 * Izzy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Izzy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Izzy.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "izzy-window.h"
#include "izzy-digi-detail.h"
#include "izzy-digi-list.h"
#include "izzy-digi-list-row.h"
#include "izzy-digi-store.h"

struct _IzzyWindow {
	GtkApplicationWindow parent_instance;

	GtkWidget *header;
	GtkWidget *btn_back;
	GtkWidget *main_stack;
	GtkWidget *digi_list;
	GtkWidget *digi_detail;
};

G_DEFINE_TYPE (IzzyWindow, izzy_window, GTK_TYPE_APPLICATION_WINDOW);

IzzyWindow *izzy_window_new(IzzyApp *app) {
	return g_object_new(IZZY_TYPE_WINDOW, "application", app, NULL);
}

static void go_back(IzzyWindow *self) {
	izzy_digi_detail_clear_info((IzzyDigiDetail *) self->digi_detail);
	gtk_widget_set_sensitive(self->btn_back, FALSE);
	gtk_stack_set_visible_child(GTK_STACK(self->main_stack), self->digi_list);
}

static void show_digi_details(IzzyDigiList *digi_list_box, gint index,
							  IzzyWindow *self) {
	IzzyDigiStore *store = izzy_digi_store_get();
	IzzyDigimon *digimon = izzy_digi_store_get_digimon_at(store,
														  (guint) index);

	izzy_digi_detail_load_info((IzzyDigiDetail *) self->digi_detail, store,
							   digimon->id);

	izzy_digi_detail_set_name(self->digi_detail, digimon->name);
	izzy_digi_detail_set_type(self->digi_detail, digimon->type);
	izzy_digi_detail_set_rank(self->digi_detail, digimon->rank);
	izzy_digi_detail_set_specialty(self->digi_detail, digimon->specialty);

	gtk_widget_set_sensitive(self->btn_back, TRUE);
	gtk_stack_set_visible_child(GTK_STACK(self->main_stack),
								self->digi_detail);
}

static void izzy_window_class_init(IzzyWindowClass *klass) {
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);

	gtk_widget_class_set_template_from_resource(widget_class,
												"/com/gitlab/kdeleteme/izzy"
												"/ui/izzy-window.ui");

	gtk_widget_class_bind_template_child(widget_class, IzzyWindow, header);
	gtk_widget_class_bind_template_child(widget_class, IzzyWindow, btn_back);
	gtk_widget_class_bind_template_child(widget_class, IzzyWindow,
										 main_stack);
	gtk_widget_class_bind_template_child(widget_class, IzzyWindow, digi_list);
	gtk_widget_class_bind_template_child(widget_class, IzzyWindow,
										 digi_detail);

	gtk_widget_class_bind_template_callback(widget_class, go_back);
	gtk_widget_class_bind_template_callback(widget_class, show_digi_details);
}

static void izzy_window_init(IzzyWindow *self) {
	g_type_ensure(IZZY_TYPE_DIGI_LIST);
	g_type_ensure(IZZY_TYPE_DIGI_DETAIL);

	gtk_widget_init_template(GTK_WIDGET(self));
	gtk_stack_set_visible_child(GTK_STACK(self->main_stack), self->digi_list);
}

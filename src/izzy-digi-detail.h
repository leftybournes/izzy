/* izzy-digi-detail.h - Shows the details of a chosen digimon
 *
 * Copyright (C) 2019  Kent Delante Kent Delante <kdeleteme@tutanota.com>
 *
 * This file is part of Izzy.
 *
 * Izzy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Izzy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Izzy. If not, see <http://www.gnu.org/licenses/>.
 */


#pragma once

#include <gtk/gtk.h>

#include "izzy-digi-store.h"

G_BEGIN_DECLS

#define IZZY_TYPE_DIGI_DETAIL (izzy_digi_detail_get_type())

G_DECLARE_FINAL_TYPE (IzzyDigiDetail,
					  izzy_digi_detail,
					  IZZY,
					  DIGI_DETAIL,
					  GtkBox);

GtkWidget *izzy_digi_detail_new(void);
void izzy_digi_detail_load_info(IzzyDigiDetail *self, IzzyDigiStore *store,
								long digimon_id);
void izzy_digi_detail_clear_info(IzzyDigiDetail *self);
void izzy_digi_detail_set_name(GtkWidget *self, const char *name);
void izzy_digi_detail_set_type(GtkWidget *self, const char *type);
void izzy_digi_detail_set_rank(GtkWidget *self, const char *rank);
void izzy_digi_detail_set_specialty(GtkWidget *self, const char *specialty);

G_END_DECLS

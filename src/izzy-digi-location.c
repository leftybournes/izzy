/* izzy-digi-location.c - Where digimon can be found
 *
 * Copyright (C) 2019  Kent Delante Kent Delante <kdeleteme@tutanota.com>
 *
 * This file is part of Izzy.
 *
 * Izzy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Izzy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Izzy. If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdlib.h>
#include <string.h>

#include "izzy-digi-location.h"

IzzyDigiLocation *izzy_digi_location_new(long id, char *floors, long digimon,
										 char *domain, bool after_bk) {

	IzzyDigiLocation *location = malloc(sizeof *location);

	location->id = id;
	location->floors = strdup(floors);
	location->digimon = digimon;
	location->domain = strdup(domain);
	location->after_bk = after_bk;

	return location;
}

void izzy_digi_location_free(IzzyDigiLocation *location) {
	free(location->floors);
	free(location->domain);
	free(location);
}

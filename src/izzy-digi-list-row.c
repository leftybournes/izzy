/* izzy-digi-list-row.c - Every row in the digi list
 *
 * Copyright (C) 2019  Kent Delante
 *
 * This file is part of Izzy.
 *
 * Izzy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Izzy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Izzy. If not, see <http://www.gnu.org/licenses/>.
 */

#include "izzy-digi-list-row.h"

struct _IzzyDigiListRow {
	GtkListBoxRow parent_instance;

	GtkWidget *label_name;
};

G_DEFINE_TYPE (IzzyDigiListRow, izzy_digi_list_row, GTK_TYPE_LIST_BOX_ROW);

static void izzy_digi_list_set_row(IzzyDigiListRow *self,
								   gchar *digimon) {
	g_autofree gchar *label_text = g_markup_printf_escaped("<big>%s</big>",
														   digimon);
	gtk_label_set_markup(GTK_LABEL(self->label_name), label_text);
}

GtkWidget *izzy_digi_list_row_new(gchar *digimon) {
	IzzyDigiListRow *self =  g_object_new(IZZY_TYPE_DIGI_LIST_ROW, NULL);
	izzy_digi_list_set_row(self, digimon);
	
	return GTK_WIDGET(self);
}

static void izzy_digi_list_row_class_init(IzzyDigiListRowClass *klass) {
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);

	gtk_widget_class_set_template_from_resource(widget_class,
												"/com/gitlab/kdeleteme/izzy/"
												"ui/izzy-digi-list-row.ui");

	gtk_widget_class_bind_template_child(widget_class, IzzyDigiListRow,
										 label_name);
}

static void izzy_digi_list_row_init(IzzyDigiListRow *self) {
	gtk_widget_init_template(GTK_WIDGET(self));
}

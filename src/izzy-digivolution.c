/* izzy-digivolution.c - Digimon digivolutions
 *
 * Copyright (C) 2019  Kent Delante <kdeleteme@tutanota.com>
 *
 * This file is part of Izzy.
 *
 * Izzy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Izzy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Izzy.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdlib.h>
#include <string.h>

#include "izzy-digivolution.h"

IzzyDigivolution *izzy_digivolution_new(long id, long digimon_id,
										long next_id, int dp_min, int dp_max,
										char *next) {
	IzzyDigivolution *digivolution = malloc(sizeof *digivolution);

	digivolution->id = id;
	digivolution->digimon_id = digimon_id;
	digivolution->next_id = next_id;
	digivolution->dp_min = dp_min;
	digivolution->dp_max = dp_max;
	digivolution->next = strdup(next);

	return digivolution;
}

void izzy_digivolution_free(IzzyDigivolution *digivolution) {
	if (digivolution->next) free(digivolution->next);
	if (digivolution) free(digivolution);
}

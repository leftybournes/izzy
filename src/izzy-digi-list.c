/* izzy-digi-list.c - Show a list of digimon
 *
 * Copyright (C) 2019  Kent Delante Kent Delante <kdeleteme@tutanota.com>
 *
 * This file is part of Izzy.
 *
 * Izzy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Izzy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Izzy. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <sqlite3.h>

#include "izzy-digimon.h"
#include "izzy-digi-list.h"
#include "izzy-digi-list-row.h"
#include "izzy-digi-store.h"

struct _IzzyDigiList {
	GtkBox parent_instance;
	
	GtkWidget *main_label;
	GtkWidget *scrolled_window;
	GtkWidget *list_box;
};

G_DEFINE_TYPE (IzzyDigiList, izzy_digi_list, GTK_TYPE_BOX);

static void populate_digi_list_callback(void *digimon,
										void *self) {
	GtkWidget *row = izzy_digi_list_row_new(((IzzyDigimon *)
												   digimon)->name);

	gtk_container_add(GTK_CONTAINER(((IzzyDigiList *) self)->list_box), row);
}

static void populate_digi_list(IzzyDigiList *self) {
	IzzyDigiStore *store = izzy_digi_store_get();
	GList *digimons = izzy_digi_store_get_digimons(store);

	g_list_foreach(digimons, populate_digi_list_callback, self);
}

GtkWidget *izzy_digi_list_new(void) {
	return GTK_WIDGET(g_object_new(IZZY_TYPE_DIGI_LIST, NULL));
}

static void izzy_digi_list_finalize(GObject *object) {
	G_OBJECT_CLASS(izzy_digi_list_parent_class)->finalize(object);
}

static guint row_activated_signal;

static void emit_row_activated_signal(GtkListBox *box, GtkListBoxRow *row,
									  IzzyDigiList *self) {
	gint index = gtk_list_box_row_get_index(row);
	
	if (index >= 0)
		g_signal_emit(self, row_activated_signal, 0, index);
}

static void izzy_digi_list_class_init(IzzyDigiListClass *klass) {
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);

	object_class->finalize = izzy_digi_list_finalize;

	gtk_widget_class_set_template_from_resource(widget_class,
												"/com/gitlab/kdeleteme/izzy"
												"/ui/izzy-digi-list.ui");

	gtk_widget_class_bind_template_child(widget_class, IzzyDigiList,
										 scrolled_window);
	gtk_widget_class_bind_template_child(widget_class, IzzyDigiList,
										 list_box);

	row_activated_signal = g_signal_new("izzy-digi-row-activated",
										G_TYPE_FROM_CLASS(object_class),
										G_SIGNAL_RUN_LAST, 0, NULL, NULL,
										NULL, G_TYPE_NONE, 1, G_TYPE_INT);

	gtk_widget_class_bind_template_callback(widget_class,
											emit_row_activated_signal);
}

static void izzy_digi_list_init(IzzyDigiList *self) {
	gtk_widget_init_template(GTK_WIDGET(self));
	populate_digi_list(self);
}

/* izzy-store.h - The database store for digimon
 *
 * Copyright (C) 2019  Kent Delante <kdeleteme@tutanota.com>
 *
 * This file is part of Izzy.
 *
 * Izzy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Izzy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Izzy.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <glib.h>
#include <sqlite3.h>

#include "config.h"

#include "izzy-digivolution.h"
#include "izzy-digi-location.h"
#include "izzy-digi-store.h"

struct _IzzyDigiStore {
	GObject parent_instance;

	GList *digimon;
	GList *digivolutions;
	GList *locations;
};

G_DEFINE_TYPE (IzzyDigiStore, izzy_digi_store, G_TYPE_OBJECT);

IzzyDigimon *izzy_digi_store_get_digimon_at(IzzyDigiStore *self, guint index)
{
	return (IzzyDigimon *) ((g_list_nth(self->digimon, index))->data);
}

GList *izzy_digi_store_get_digimons(IzzyDigiStore *self) {
	return self->digimon;
}

IzzyDigiStore *izzy_digi_store_new(void) {
	return g_object_new(IZZY_TYPE_DIGI_STORE, NULL);
}

IzzyDigiStore *izzy_digi_store_get(void) {
	static IzzyDigiStore *digi_store;

	if (!digi_store) {
		digi_store = izzy_digi_store_new();
	}

	return digi_store;
}

static sqlite3 *get_db() {
	sqlite3 *db;

	g_autofree gchar *db_dir = g_build_path("/", PKGDATADIR, "izzy.db", NULL);
	int result = sqlite3_open_v2(db_dir, &db, SQLITE_OPEN_READONLY, NULL);

	if (result != SQLITE_OK) {
		g_error("Error opening db: Result %d", result);
		sqlite3_close(db);
		return NULL;
	}

	return db;
}

static void izzy_digi_store_finalize(GObject *object) {
	IzzyDigiStore *self = (IzzyDigiStore *) object;

	g_list_free_full(self->digimon, (void *) izzy_digimon_free);

	G_OBJECT_CLASS(izzy_digi_store_parent_class)->finalize(object);
}

static void izzy_digi_store_init(IzzyDigiStore *self) {}

static void izzy_digi_store_class_init(IzzyDigiStoreClass *klass) {
	GObjectClass *object_class = G_OBJECT_CLASS(klass);

	object_class->finalize = izzy_digi_store_finalize;
}

static int load_all_callback(void *store, int columns, char **data,
							 char **col_name) {
	IzzyDigiStore *self = (IzzyDigiStore *) store;

	IzzyDigimon *digimon = izzy_digimon_new(atol(data[0]), data[1], data[2],
										   data[3], data[4]);

	self->digimon = g_list_append(self->digimon, digimon);
	
	return 0;
}

void izzy_digi_store_load_all(IzzyDigiStore *self) {
	sqlite3 *db = get_db();
	char *err_msg = NULL;

	if (!db) return;

	char *sql = "SELECT * FROM digimon";

	int result = sqlite3_exec(db, sql, load_all_callback, self, &err_msg);

	if (result != SQLITE_OK) {
		g_error("%s", err_msg);
		sqlite3_free(err_msg);
		sqlite3_close(db);
		return;
	}

	sqlite3_free(err_msg);
	sqlite3_close(db);
}

static int load_locations_callback(void *store, int columns, char **data,
								  char **col_name) {
	IzzyDigiStore *self = (IzzyDigiStore *) store;
	
	bool after_bk = false;

	if (atoi(data[4]) == 1) after_bk = true;

	IzzyDigiLocation *location = izzy_digi_location_new(atol(data[0]),
														data[1],
														atol(data[2]),
														data[3], after_bk);

	self->locations = g_list_append(self->locations, location);

	return 0;
}

static void load_locations(IzzyDigiStore *self, long digimon_id) {
	sqlite3 *db = get_db();

	if (!db) return;

	g_autofree char *sql = g_strdup_printf("SELECT location_id, floors, "
										   "digimon, domain, after_bk "
										   "FROM v_locations "
										   "WHERE digimon = %ld", digimon_id);

	if (self->locations != NULL)
		for (GList *l = self->locations; l != NULL; l = l->next) {
			izzy_digi_location_free((IzzyDigiLocation *) l->data);
			self->locations = g_list_delete_link(self->locations, l);
		}
	
	char *err_msg = NULL;

	int result = sqlite3_exec(db, sql, load_locations_callback, self,
							  &err_msg);

	if (result != SQLITE_OK) {
		g_error("%s\n", err_msg);
		sqlite3_free(err_msg);
		sqlite3_close(db);
		return;
	}

	sqlite3_free(err_msg);
	sqlite3_close(db);
}

GList *izzy_digi_store_get_locations(IzzyDigiStore *self, long digimon_id) {
	load_locations(self, digimon_id);

	return self->locations;
}

static int load_digivolutions_callback(void *store, int columns,
									   char **data, char **col_name) {
	IzzyDigiStore *self = (IzzyDigiStore *) store;

	int dp_max;
	if (data[4]) dp_max = atoi(data[4]);
	else dp_max = 0;

	IzzyDigivolution *digivolution = izzy_digivolution_new(atol(data[0]),
														   atol(data[1]),
														   atol(data[2]),
														   atoi(data[3]),
														   dp_max, data[5]);
	
	self->digivolutions = g_list_append(self->digivolutions, digivolution);

	return 0;
}

static void load_digivolutions(IzzyDigiStore *self, long digimon_id) {
	sqlite3 *db = get_db();
	char *err_msg = NULL;

	if (!db) return;

	g_autofree char *sql = g_strdup_printf("SELECT digivolution_id, "
										   "digimon_id, next_id, "
										   "digivolve_point_min, "
										   "digivolve_point_max, next "
										   "FROM v_digivolutions "
										   "WHERE digimon_id = %ld",
										   digimon_id);

	if (self->digivolutions) {
		for (GList *d = self->digivolutions; d; d = d->next) {
			izzy_digivolution_free((IzzyDigivolution *) d->data);
			self->digivolutions = g_list_delete_link(self->digivolutions, d);
		}

		g_list_free(self->digivolutions);
	}

	int result = sqlite3_exec(db, sql, load_digivolutions_callback,
							  self, &err_msg);

	if (result != SQLITE_OK) {
		g_error("%s", err_msg);
		sqlite3_free(err_msg);
		sqlite3_close(db);
		return;
	}

	sqlite3_free(err_msg);
	sqlite3_close(db);
}

GList *izzy_digi_store_get_digivolutions(IzzyDigiStore *self,
										 long digimon_id) {
	load_digivolutions(self, digimon_id);

	return self->digivolutions;
}

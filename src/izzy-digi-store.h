/* izzy-store.h - The database store for digimon
 *
 * Copyright (C) 2019  Kent Delante <kdeleteme@tutanota.com>
 *
 * This file is part of Izzy.
 *
 * Izzy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Izzy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Izzy.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

#include "izzy-digimon.h"

G_BEGIN_DECLS

#define IZZY_TYPE_DIGI_STORE (izzy_digi_store_get_type())

G_DECLARE_FINAL_TYPE (IzzyDigiStore, izzy_digi_store, IZZY, DIGI_STORE,
					  GObject);

IzzyDigiStore *izzy_digi_store_new(void);
IzzyDigiStore *izzy_digi_store_get(void);
void izzy_digi_store_load_all(IzzyDigiStore *self);
GList *izzy_digi_store_get_digimons(IzzyDigiStore *self);
IzzyDigimon *izzy_digi_store_get_digimon_at(IzzyDigiStore *self, guint index);
GList *izzy_digi_store_get_digivolutions(IzzyDigiStore *self,
										 long digimon_id);
GList *izzy_digi_store_get_locations(IzzyDigiStore *self, long digimon_id);

G_END_DECLS

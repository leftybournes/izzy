/* izzy-window.h
 *
 * Copyright (C) 2019  Kent Delante <kdeleteme@tutanota.com>
 *
 * This file is part of Izzy.
 *
 * Izzy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Izzy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Izzy.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "izzy-app.h"

G_BEGIN_DECLS

#define IZZY_TYPE_WINDOW (izzy_window_get_type())

G_DECLARE_FINAL_TYPE (IzzyWindow,
					  izzy_window,
					  IZZY,
					  WINDOW,
					  GtkApplicationWindow);

IzzyWindow *izzy_window_new(IzzyApp *app);
	
G_END_DECLS

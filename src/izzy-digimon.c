/* izzy-digimon.c - Schema for digimon
 *
 * Copyright (C) 2019  Kent Delante Kent Delante <kdeleteme@tutanota.com>
 *
 * This file is part of Izzy.
 *
 * Izzy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Izzy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Izzy. If not, see <http://www.gnu.org/licenses/>.
 */

#include "izzy-digimon.h"

#include <stdlib.h>
#include <string.h>

IzzyDigimon *izzy_digimon_copy(IzzyDigimon from) {
	IzzyDigimon *to = malloc(sizeof *to);
	to->id = from.id;

	to->name = strdup(from.name);
	to->type = strdup(from.type);
	to->rank = strdup(from.rank);
	to->specialty = strdup(from.specialty);

	return to;
}

IzzyDigimon *izzy_digimon_new(long id, char *name, char *type, char *rank,
							   char *specialty) {
	IzzyDigimon *digimon = malloc(sizeof *digimon);


	digimon->id = id;
	digimon->name = strdup(name);
	digimon->type = strdup(type);
	digimon->rank = strdup(rank);
	digimon->specialty = strdup(specialty);

	return digimon;
}

void izzy_digimon_free(IzzyDigimon *digimon) {
	free(digimon->name);
	free(digimon->type);
	free(digimon->rank);
	free(digimon->specialty);
	free(digimon);
}

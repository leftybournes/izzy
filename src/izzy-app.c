/* izzy-app.c
 *
 * Copyright (C) 2019  Kent Delante <kdeleteme@tutanota.com>
 *
 * This file is part of Izzy.
 *
 * Izzy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Izzy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Izzy.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "izzy-app.h"
#include "izzy-window.h"
#include "izzy-digi-store.h"

struct _IzzyApp {
	GtkApplication parent_instance;

	GtkCssProvider *css_provider;
};

G_DEFINE_TYPE (IzzyApp, izzy_app, GTK_TYPE_APPLICATION);

IzzyApp *izzy_app_new(void) {
	return g_object_new(IZZY_TYPE_APP,
						"application-id", "com.gitlab.kdeleteme.Izzy",
						"flags", G_APPLICATION_FLAGS_NONE,
						NULL);
}

static void load_css(GApplication *g_app) {
	IzzyApp *app = IZZY_APP(g_app);
	
	if (!app->css_provider) {
		app->css_provider = gtk_css_provider_new();
	}

	gtk_css_provider_load_from_resource(app->css_provider,
										"/com/gitlab/kdeleteme/izzy/ui/style/"
										"izzy.css");

	
	gtk_style_context_add_provider_for_screen(gdk_screen_get_default(),
											  GTK_STYLE_PROVIDER(app->css_provider),
											  GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
}

static void izzy_app_activate(GApplication *app) {
	load_css(app);
	
	GtkApplicationWindow *win;
	win = GTK_APPLICATION_WINDOW(izzy_window_new(IZZY_APP(app)));

	gtk_window_present(GTK_WINDOW(win));
}

static void izzy_app_init(IzzyApp *self) {
	IzzyDigiStore *store = izzy_digi_store_get();
	izzy_digi_store_load_all(store);
}

static void izzy_app_class_init(IzzyAppClass *klass) {
	GApplicationClass *application_class = G_APPLICATION_CLASS(klass);

	application_class->activate = izzy_app_activate;
}
